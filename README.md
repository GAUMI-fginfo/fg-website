# Metarepo für die Fachgruppenwebseite

[![meta pipeline status](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website/badges/master/pipeline.svg)](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website/-/commits/master)
[![pipeline status](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data/badges/main/pipeline.svg)](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data/-/commits/main)

Webseite: https://fg.informatik.uni-goettingen.de/

Inhaltsrepo: https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data

[TOC]

## Was ist in diesem Repo?

Dieses Repo beschreibt wie die Fachgruppenwebseite gebaut wird und baut diese auch.

Den eigentlichen Inhalt findest du im [Inhaltsrepo](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data).


## Seite Lokal Hosten

Um an der Seite zu arbeiten ist es meistens sinnvoll diese auch lokal zu hosten.

### Setup

Die folgenden Befehle sind für Ubuntu 20.04 LTS (Focal) gedacht.

1. Pakete installieren: `apt-get update && apt-get install -y --no-install-recommends python3-pip python3 python3-venv make git wget software-properties-common`
1. Metarepo klonen: `git clone git@gitlab.gwdg.de:GAUMI-fginfo/fg-website.git`
1. `cd fg-website`
1. Inhaltsrepo klonen: `git clone git@gitlab.gwdg.de:GAUMI-fginfo/fg-website-data.git content`

### (Automatisch) Bauen und Hosten

Die Seite kann nun relativ einfach mit `make` gebaut werden.
Dabei wird die [config.json](config.json) Datei als Config verwendet.
Die Ausgabe befindet sich im `output` Ordner.

Um die Seite lokal zu hosten kann `make devserver` verwendet werden.
Die Seite kann man dann unter [http://127.0.0.1:8000](http://127.0.0.1:8000) finden.
Die Seite wird **nicht** automatisch neu gebaut. Man muss also `make` manuell ausführen um die Seite neuzubauen.
