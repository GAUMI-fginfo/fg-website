# Macros

[TOC]

## [getters.html](getters.html)

```
{%- import 'macros/getters.html' as get with context -%}
```

Mit diesen Macros können Informationen abgefragt werden.

### `category_by_name(catname, ignore_native = False)`

**Argumente:**

- `catname` (String): Der Name der Kategorie.
- `ignore_native` (Boolean): Setzt den ersten und dritten Rückgabewert zu `None`. (Verkürzt die Laufzeit dieser Funktion.)

**Rückgabewerte:**

1. ([Category](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#object-author-cat-tag)): Die native Kategorie.
2. (Dict): Der Eintrag zu der Kategorie aus der config.json-Datei aus dem Inhaltsrepo.
3. (List\<[Article](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#article)\>): Die Artikel in der Kategorie.

**Beispiel Aufruf:**

```
{%- call(nativecat, sccat, catarticles) get.category_by_name(article.category.name) -%}
	<ul>
		<li>Kategorie: {{ nativecat.name }}</li>
		<li>Farbe: {{ sccat.color }}</li>
		<li>Anzahl Artikel: {{ catarticles|length }}</li>
	</ul>
{%- endcall -%}
```

### `tag_by_name(tagname)`

**Argumente:**

- `catname` (String): Der Name des Tags.

**Rückgabewerte:**

1. ([Tag](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#object-author-cat-tag)): Der native Tag.
2. (Dict): Falls der Tag gleichnamig zu einer Kategorie ist, dann der Eintrag zu der Kategorie aus der config.json-Datei aus dem Inhaltsrepo, sonst `None`.
3. (List\<[Article](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#article)\>): Die Artikel die diesen Tag haben.

**Beispiel Aufruf:**

```
{%- call(nativetag, sccat, tagarticles) get.tag_by_name("event") -%}
	<ul>
		<li>Tag: {{ nativetag.name }}</li>
		<li>Farbe: {% if sccat %}{{ sccat.color }}{% else %}Dieser Tag ist nicht gleichnamig zu einer Kategorie und hat deshalb keine Farbe.{% endif %}</li>
		<li>Anzahl Artikel mit diesem Tag: {{ tagarticles|length }}</li>
	</ul>
{%- endcall -%}
```


### `article_by_slug(slug, lang)`

**Argumente:**

- `slug` (String): Der Slug von dem Artikel.
- `lang` (String): Die Sprache von dem Artikel.

**Rückgabewerte:**

1. ([Article](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#article)): Der Artikel.

### `page_by_slug(slug, lang)`

**Argumente:**

- `slug` (String): Der Slug von der Seite.
- `lang` (String): Die Sprache von der Seite.

**Rückgabewerte:**

1. ([Page](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#page)): Die Seite.

### `article_or_page_by_slug(slug, lang)`

**Argumente:**

- `slug` (String): Der Slug von dem Artikel oder der Seite.
- `lang` (String): Die Sprache von dem Artikel oder der Seite.

**Rückgabewerte:**

1. ([Article](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#article) | [Page](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#page)): Der Artikel oder die Seite.
2. (String): "article" falls es ein Artikel ist. "page" falls es eine Seite ist.

## [cards.html](cards.html)

```
{%- import 'macros/cards.html' as cards with context -%}
```

Mit diesen Macros können Kachelblöcke erstellt werden.

In einem Kachelblock dürfen nur Kacheln sein und sonst nichts!

**Beispiel:**

```
{{ cards.open() }}

{%- for article in all_articles -%}
	{{ cards.card_from_article_or_page(article) }}
{%- endfor -%}

{{ cards.card(title="Dies ist auch eine Kachel", url="https://www.youtube.com/watch?v=dQw4w9WgXcQ", catcolor="green") }}

{{ cards.close() }}
```

### `open(id = None, classes = None)`

Beginnt einen Kachelblock.

**Argumente:**

- `id` (String): Die ID von dem Kachelblock.
- `classes` (List\<String\>): Zusätzliche Klassen für den Kachelblock.

### `close()`

Schließt einen Kachelblock.

### `card(title, url, catcolor, escape_title = True, is_url_external = True)`

Erstellt eine Kachel.

**Argumente:**

- `title` (String): Der Titel.
- `url` (String): Der URL.
- `catcolor` (String): Die Farbe.
- `escape_title` (Boolean): Ob `title` escaped werden soll.
- `is_url_external` (Boolean): Ob `url` auf eine externe Resource zeigt, oder als relativen Link interpretiert werden soll.

### `card_from_article_or_page(aop)`

Erstellt eine Kachel von einem Artikel oder einer Seite.

**Argumente:**

1. `aop` ([Article](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#article) | [Page](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#page)): Der Artikel oder die Seite.


### `cards_from_articles_or_pages(asops, max = None, standalone = True)`

Erstellt mehrere Kacheln oder einen vollständigen Kachelblock von mehreren Artikeln oder Seiten.

**Argumente:**

1. `asops` (List\<[Article](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#article) | [Page](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/themes.html#page)\>): Die Artikel oder Seiten. Kann auch gemischt sein.
2. `max` (Int | None): Falls vorhanden werden maximal `max` Kacheln gerendert.
3. `standalone` (Boolean): Ob `open()` und `close()` automatisch mit ausgeführt werden soll.

**Beispiel Aufruf:**

```
{{ cards_from_articles_or_pages(all_articles, max=5) }}
```

## [renderers.html](renderers.html)

```
{%- import 'macros/renderers.html' as render with context -%}
```

**TODO: ** Die Dokumentation für diese Macros ist abhängig von der Dokumentation von der config.json-Datei im Inhaltsrepo.

### `section_news(s)`
### `section_custom(s)`
### `section_iframe(s)`
### `section_category(s)`
### `section_tag(s)`
### `section(s)`
