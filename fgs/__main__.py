#!/usr/bin/env python3

import json
import os
import sys
import mimetypes


import common
import datatypes
import reader
import generator
import writer

#content_dir = '../content'
#output_dir = '../output'
#theme_dir = '../theme'
#config_file = '../config.json'
#lang_file = '../lang.json'


def main(content_dir, output_dir, theme_dir, config_file, lang_file):
    print("Hello World")
    config = {}
    with open(config_file) as f:
        config = common.combine(config, json.loads(f.read()))
    with open(lang_file) as f:
        if 'lang' not in config:
            config['lang'] = {}
        config['lang'] = common.combine(config['lang'], json.loads(f.read()))
    with open(content_dir + '/config.json') as f:
        config = common.combine(config, json.loads(f.read()))
    print(config)

    factories = {}
    factories['page'] = datatypes.PageFactory(config, factories)
    factories['author'] = datatypes.AuthorFactory(config, factories)
    factories['tag'] = datatypes.TagFactory(config, factories)
    factories['link'] = datatypes.LinkFactory(config, factories)
    factories['config'] = datatypes.LocalizedConfigFactory(config, factories)
    factories['file'] = datatypes.FileFactory(config, factories)


    mimetypes.init()

    readers = []
    readers.append(reader.RawFileReader(config, factories))
    for mimetype, mimeconfig in config['pandoc']['mimetypes'].items():
        extensions = config['pandoc']['extensions']
        if 'extensions' in mimeconfig:
            extensions = mimeconfig['extensions']
        readers.append(reader.PandocReader(config, factories, mimetype, None, base=mimeconfig['base'], extensions=extensions))

    read_dir(os.path.join(content_dir, "."), readers)
    read_dir(theme_dir + '/static', readers, [config['theme']['static_dir']])


    context = {}
    gen = generator.Generator(config, context, factories)
    gen.generate_context()

    wrt = writer.Writer(config, context, output_dir, theme_dir)

    gen.generate_output(wrt)



def read_dir(directory, readers, subpath = []):
    print("read_dir: " + directory);
    for filename in os.listdir(directory):
        if filename.startswith("."):
            continue
        f = os.path.join(directory, filename)
        if os.path.isfile(f):
            mimetype, mimeencoding = mimetypes.guess_type(f)
            #print("mimetype:",f,mimetype,mimeencoding)
            read_file = False
            for reader in readers:
                if reader.can_read_file(mimetype, mimeencoding):
                    print("read file: ", f, subpath, mimetype, mimeencoding, reader.__class__)
                    read_file = True
                    reader.read_file(f, subpath, mimetype, mimeencoding)
            if not read_file:
                print("WARN: no reader for file: ", f, subpath, mimetype, mimeencoding)

            #readers['generic'].read_file(f, subpath)
            #if filename.endswith(".md"):
            #    readers['md'].read_file(f, subpath)
        elif os.path.isdir(f):
            read_dir(f, readers, subpath + [filename])




if __name__ == '__main__':
    argv = sys.argv
    if len(argv) != 6:
        print("Usage:",argv[0],"<content_dir> <output_dir> <theme_dir> <config_file> <lang_file>")
        os._exit(1)
    main(argv[1], argv[2], argv[3], argv[4], argv[5])

