from jinja2 import Environment, FileSystemLoader, StrictUndefined

import os

import common

class Writer:

    def __init__(self, config, context, output_dir, theme_dir):
        self.config = config
        self.context = context
        self.output_dir = output_dir
        self.theme_dir = theme_dir

        self.env = Environment(
            loader=FileSystemLoader(theme_dir + "/templates"),
            autoescape=False,
            undefined = StrictUndefined
        )

        print("templates: ", self.env.list_templates())

    def write_template(self, template, path, lang, extra_context, localized_config):
        #print("write_template:", template, path, lang, extra_context, localized_config)
        tmpl = self.env.get_template(template)

        pathsplit = path.split('/')
        #pathsplit.remove('.') # TODO remove all not just one
        #pathsplit.remove('..') # TODO remove all not just one

        siteurl = "."
        if self.config['relative_urls']:
            count = len(pathsplit) - 1
            for i in range(count):
                siteurl = siteurl + '/..'
        else:
            siteurl = self.config['siteurl']

        # render template
        context = {}
        context = common.combine(context, self.context)
        context["config"] = localized_config
        context["theme"] = localized_config['theme']
        context["template"] = template
        context["t"] = localized_config["lang"] # translate
        context["l"] = lang # current language
        context["path"] = path
        context["siteurl"] = siteurl
        context = common.combine(context, extra_context)
        #print("template: ", template)
        #print("path: ", path)
        #print("lang: ", lang)
        #print("context: ", context)
        #print("context keys: ", context.keys())
        #print("tags:", type(context['tags']), context['tags'])
        out = tmpl.render(context)

        # write file
        self.write_file(path, out)

    def write_file(self, path, out, mode="w"):
        # write to file
        fullpath = self.output_dir + '/' + path
        directory = os.path.dirname(fullpath)
        if os.path.exists(fullpath):
            readmode = 'r'
            if 'b' in mode:
                readmode = 'rb'
            with open(fullpath, readmode) as f:
                if out == f.read():
                    print("skipping writing file: ", fullpath)
                    return
        print("writing file: ", fullpath)
        #print("dir: ", directory)
        os.makedirs(directory, exist_ok=True)
        with open(fullpath, mode) as f:
            f.write(out)


