
def combine(old, new):
    if type(old) != type(new):
        raise Exception("Cannot combine different types: ", type(old), type(new), old, new)
    if isinstance(old, dict):
        res = old.copy()
        for key, value in new.items():
            if key in res:
                res[key] = combine(res[key], value)
            else:
                res[key] = value
        return res
    else:
        return new
