.PHONY: html
html: docs build

.PHONY: publish
publish: build

.PHONY: build
build: venv/bin/activate pandoc mathjax
	. venv/bin/activate && cd fgs && python3 __main__.py "../content" "../output" "../theme" "../config.json" "../lang.json"

.PHONY: docs
docs: venv/bin/activate pandoc mathjax
	. venv/bin/activate && cd fgs && python3 __main__.py "../docs" "../docs-output" "../theme" "../config.json" "../lang.json"

.PHONY: mathjax
mathjax:
	git submodule init
	git submodule update
	[ ! -d output/mathjax ] && mkdir -p output && cp -vr mathjax/es5 output/mathjax || true

.PHONY: devserver
devserver: venv/bin/activate
	. venv/bin/activate && python3 -m http.server 8000

.PHONY: clean
clean:
	$(RM) -r venv output

.PHONY: pandoc
pandoc: pandoc-2.18
	ln -svf pandoc-2.18 pandoc

pandoc-2.18:
	[ ! -x pandoc-2.18/bin/pandoc ] \
	&& wget https://github.com/jgm/pandoc/releases/download/2.18/pandoc-2.18-linux-amd64.tar.gz -O pandoc.tar.gz \
	&& tar xzvf pandoc.tar.gz \
	&& rm -v pandoc.tar.gz \
	|| true
	[ -x pandoc-2.18/bin/pandoc ]

venv/bin/activate: requirements.txt
	python3 -m venv venv
	. venv/bin/activate && pip3 install -r requirements.txt

