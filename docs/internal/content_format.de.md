---
title: Content Format
---



## Attr


| Key | Parser | Comment |
|-----|--------|---------|
| id | String |  |
| classes | \[String\] |  |
| extra | {foo: bar, alice: bob, ...} |  |


## Blocks


### header

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `header` |  |
| eclass | `block` |  |
| level | Integer |  |
| attr | [Attr](#attr) |  |
| content | \[[Inline](#inlines)\] |  |


### rawblock

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `rawblock` |  |
| eclass | `block` |  |
| format | String |  |
| raw | String |  |


### bulletlist

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `bulletlist` |  |
| eclass | `block` |  |
| items | \[\[[Block](#blocks)\]\] |  |
| count | Integer |  |


### orderedlist

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `orderedlist` |  |
| eclass | `block` |  |
| items | \[\[[Block](#blocks)\]\] |  |
| count | Integer |  |
| start | Integer |  |
| style | `default` \| `example` \| `decimal` \| `lower_roman` \| `upper_roman` \| `lower_alpha` \| `upper_alpha` |  |
| delim | `default` \| `period` \| `one_parenthesis` \| `two_parentheses` |  |


### blockquote

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `blockquote` |  |
| eclass | `block` |  |
| content | \[[Block](#blocks)\] |  |


### plain

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `plain` |  |
| eclass | `block` |  |
| content | \[[Inline](#inlines)\] |  |


### paragraph

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `paragraph` |  |
| eclass | `block` |  |
| content | \[[Inline](#inlines)\] |  |


### codeblock

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `codeblock` |  |
| eclass | `block` |  |
| attr | [Attr](#attr) |  |
| code | String |  |
| code_lines | \[String\] |  |


### horizontalrule

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `horizontalrule` |  |
| eclass | `block` |  |


### blockcontainer

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `blockcontainer` |  |
| eclass | `block` |  |
| attr | [Attr](#attr) |  |
| content | \[[Block](#blocks)\] |  |


## Inlines


### space

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `space` |  |
| eclass | `inline` |  |


### linebreak

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `linebreak` |  |
| eclass | `inline` |  |


### softbreak

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `softbreak` |  |
| eclass | `inline` |  |


### string

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `string` |  |
| eclass | `inline` |  |
| text | String |  |


### strong

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `strong` |  |
| eclass | `inline` |  |
| content | \[[Inline](#inlines)\] |  |


### emph

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `emph` |  |
| eclass | `inline` |  |
| content | \[[Inline](#inlines)\] |  |


### underline

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `underline` |  |
| eclass | `inline` |  |
| content | \[[Inline](#inlines)\] |  |


### strikeout

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `strikeout` |  |
| eclass | `inline` |  |
| content | \[[Inline](#inlines)\] |  |


### superscript

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `superscript` |  |
| eclass | `inline` |  |
| content | \[[Inline](#inlines)\] |  |


### subscript

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `subscript` |  |
| eclass | `inline` |  |
| content | \[[Inline](#inlines)\] |  |


### smallcaps

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `smallcaps` |  |
| eclass | `inline` |  |
| content | \[[Inline](#inlines)\] |  |


### link

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `link` |  |
| eclass | `inline` |  |
| attr | [Attr](#attr) |  |
| content | \[[Inline](#inlines)\] |  |
| url | String |  |
| title | String |  |


### image

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `image` |  |
| eclass | `inline` |  |
| attr | [Attr](#attr) |  |
| alt | \[[Inline](#inlines)\] |  |
| url | String |  |
| title | String |  |


### quoted

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `quoted` |  |
| eclass | `inline` |  |
| quotetype | `single` \| `double` |  |
| content | \[[Inline](#inlines)\] |  |


### math

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `math` |  |
| eclass | `inline` |  |
| mathtype | `display` \| `inline` |  |
| math | String |  |


### code

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `code` |  |
| eclass | `inline` |  |
| attr | [Attr](#attr) |  |
| code | String |  |
| code_lines | \[String\] |  |


### inlinecontainer

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `inlinecontainer` |  |
| eclass | `inline` |  |
| attr | [Attr](#attr) |  |
| content | \[[Inline](#inlines)\] |  |


### rawinline

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `rawinline` |  |
| eclass | `inline` |  |
| format | String |  |
| raw | String |  |


### footnote

| Key | Parser | Comment |
|-----|--------|---------|
| etype | `footnote` |  |
| eclass | `inline` |  |
| content | \[[Block](#blocks)\] |  |
