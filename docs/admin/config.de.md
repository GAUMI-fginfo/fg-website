---
title: "Konfiguration"
tags: content
---

:::warning
TODO
:::

## Menüleiste & Startseite anpassen

Die Menüleiste und die Startseite werden über die [config.json](config.json) Datei konfiguriert.

Die Menüleiste kann (derzeitig nur) auf Kategorien und Artikel/Seiten verlinken. Um auf eine Seite in der Menüleiste zu verlinken, muss diese im Markdown Header, muss diese im Markdown Header den `slug` Wert gesetzt haben.

