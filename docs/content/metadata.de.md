---
title: "Yaml Metadatenblock"
prio: 3
---

Eine jede Markdown Datei **muss** mit einem Metadaten Header anfangen.
Dieser muss mindestens `title` gesetzt haben.

Ein Metadaten Header sieht z.B. wie folgt aus:
```markdown
---
title: Mein toller Artikel
slug: mein_toller_artikel
status: published
tags: droggelbecher,foo,bar,hallo,welt
lang: de
---
```

Weiter Informationen zu möglichen Metadaten gibt es in der [Pelican Dokumentation](https://gaumi-fginfo.pages.gwdg.de/pelican/_build/html/content.html#file-metadata).


