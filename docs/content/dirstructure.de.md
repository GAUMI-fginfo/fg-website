---
title: "Dateien, Kategorien & Ordnerstruktur"
prio: 2
---

:::info
Im [Inhaltsrepo](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data) liegen die Inhalte für die Fachgruppenwebseite.
Auf dieser Seite findest du alle Informationen über die Struktur vom Inhaltsrepo.
:::

Für die Konfiguration gibt es die [config.json Datei](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data/-/blob/main/config.json), worüber z.B. das Seitenmenü konfiguriert werden kann. Für weitere Informationen zur Konfiguration und was alles in diese Datei kann, siehe [hier](slug:config).

Ordner und Dateien die mit `.` anfangen werden beim Generieren der Webseite ignoriert.

:::danger
Alle Dateinamen im Inhaltsrepo müssen eindeutig sein!
Heißt es darf nicht zwei Dateien mit gleichem Namen in unterschiedlichen Ordnern geben.
Falls Dateien mit gleichem Namen existieren, kann die Webseite nicht gebaut werden.
(Außerdem darf es auch keine Dateien mit dem gleichen Namen einer Datei aus dem [statischen Teil des Themes](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website/-/tree/master/theme/static) geben.)
:::

## Inhaltsdateien

Inhaltsdateien sind die Dateien aus denen die einzelnen Seiten mit Inhalt generiert werden.

Diese Dateien sind häufig [Markdown](slug:mdsyntax)dateien und folgen häufig dem Namensschema `<slug>.<lang>.md` (z.B. `imprint.de.md`).

Sie können entweder im Stammverzeichnis vom Inhaltsrepo liegen, oder in einem Unterordner(einer [Kategorie](#Kategorien)).

Weitere Informationen zum Erstellen und Schreiben von Inhaltsdateien findest du [hier](slug:createpage).


## Kategorien

Alle sonstigen Ordner repräsentiern eine Kategorie. Kategorien werden in der [config.json](config.json) Datei konfiguriert.

Dateien die in gar keinem Ordner abgelegt werden (also z.B. diese [README.md](README.md)), werden implizit in die `misc` Kategorie getan.


