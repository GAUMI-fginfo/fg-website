---
title: "Markdown Syntax"
prio: 4
---

:::info
Die Markdown Syntax baut auf der [Markdown Syntax von Pandoc](https://pandoc.org/MANUAL.html#pandocs-markdown) auf. Es sind einige Extensions aktiviert, um die Syntax möglichst ähnlich zur [Hedgedoc Markdown Syntax](https://pad.gwdg.de/features) zu machen.

Die vollständige Markdown Syntax die verwendet werden kann ist also sehr umfangreich. Dieses Dokument *sollte* alle Features und Schreibweisen aufzeigen.
:::

Markdowncodebeispiele zusammen mit den entsprechenden Ergebnissen die generiert wurden sehen in diesem Dokument wie folgt aus:
```markdown
Ich bin ein *Markdown* Beispiel.
```
:::success
Ich bin ein *Markdown* Beispiel.
:::

## Inhaltsverzeichnis

Ein Paragraph mit einzigem Inhalt `[TOC]` generiert ein Inhaltsverzeichnis.

```markdown

[TOC]

```
:::success

[TOC]

:::


## Paragraphen

Ein Paragraph besteht aus einer oder mehr Zeilen an Text gefolgt von einer oder mehreren Leerzeilen.
Ein Zeilenumbruch in einem Paragraphen wird, wie in Hedgedoc auch, direkt auf der Webseite als Zeilenumbruch übernommen.

```markdown
Dies ist der erste Paragraph.

Dies ist der zweite Paragraph und dieser Paragraph
beinhaltet sogar einen Zeilenumbruch.
```
:::success
Dies ist der erste Paragraph.

Dies ist der zweite Paragraph und dieser Paragraph
beinhaltet sogar einen Zeilenumbruch.
:::

## Überschriften

Überschriften werden genutzt um ein Dokument zu strukturieren.

Es gibt zwei Arten Überschriften zu schreiben: [Setext](#setext) und [ATX](#atx).
In Überschriften kann [Inlinemarkup](#inlinemarkup) verwendet werden.
Eine Leerzeile vor oder nach einer Überschrift ist nicht notwendig.

### Nutzungsinformationen

- Jede Seite wird automatisch mit einer Level 1 Überschrift aus dem Titel generiert.
  Entsprechend sollten die meisten Dokumente nur Level 2-6 Überschriften verwenden!
- Überschriften haben semantische Bedeutung welche von Browsern, Bots und Screenreadern interpretiert und verwendet wird.
  Überschriften sollten deshalb _niemals_ verwendet werden um Text einfach nur **fettgedruckt** oder in einer bestimmten Schriftgröße zu schreiben weil es ja "gut aussehen würde".
- Es sollte vermieden werden Überschriftenlevel zu überspringen. Siehe [hier](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements#navigation) für eine Erklärung.



### Setext


Setext-Style Überschriften bestehen jeweils aus einer Zeile mit dem Überschriftstext gefolgt von einer Zeile mit `=`-Symbolen (für eine Level 1 Überschrift) oder  `-`-Symbolen (für eine Level 2 Überschrift).

```markdown
Eine Level 1 Überschrift
========================

Eine Level 2 Überschrift
------------------------
```
:::success
Eine Level 1 Überschrift
========================

Eine Level 2 Überschrift
------------------------
:::


### ATX

ATX-Style Überschriften bestehen jeweils aus einer Zeile mit ein bis sechs `#`-Symbolen gefolgt von mindestens einem Leerzeichen, dem jeweiligen Überschriftstext und einer optionalen Menge an weiteren `#`-Symbolen.
Die Anzahl der `#`-Symbole am Anfang der Zeile gibt das Level an.

```markdown
# Eine Level 1 Überschrift
## Eine Level 2 Überschrift
### Eine Level 3 Überschrift ###
#### Eine Level 4 Überschrift
##### Eine Level 5 Überschrift
###### Eine Level 6 Überschrift
```
:::success
# Eine Level 1 Überschrift
## Eine Level 2 Überschrift
### Eine Level 3 Überschrift ###
#### Eine Level 4 Überschrift
##### Eine Level 5 Überschrift
###### Eine Level 6 Überschrift
:::

### Identifikator, Attribute und Klassen

Am Ende der Zeile mit dem Überschriftentext kann man auch optionale Metadaten hinzugefügen.

Dafür schreibt man leerzeichengetrennt in geschweiften Klammern die folgenden Dinge:
- den Identifikator: `#Identifikator` (maximal einmal)
- die Klassen: `.eine-klasse .eine-andere-klasse .noch-eine-klasse`
	- Die Klasse `.unnumbered` makiert eine Überschrift als "soll nicht nummeriert werden". Als Alias reicht auch ein `-`.
	- Die Klasse `.unlisted` makiert eine Überschrfit als "soll im Inhaltsverzeichnis nicht eingetragen werden".

In den meisten Fällen wird maximal der Identifikator benötigt.
Der Identifaktor kann zum verlinken des entsprechenden Abschnittes verwendet werden.

```markdown
#### Eine Überschrift {#bspueberschrift}
#### Eine andere Überschrift {#anderebspueberschrift .mit-klassen .foobar}
#### Nich Nummeriert und nicht im Inhaltsverzeichnis {- .unlisted}
```
:::success
#### Eine Überschrift {#bspueberschrift}
#### Eine andere Überschrift {#anderebspueberschrift .mit-klassen .foobar}
#### Nich Nummeriert und nicht im Inhaltsverzeichnis {- .unlisted}
:::

:::info
Falls kein Identifikator für eine Überschrift gesetzt wurde, wird automatisch einer aus dem Überschriftentext generiert.

:::danger
TODO https://pandoc.org/MANUAL.html#headings-and-sections
:::
:::

## Inlinemarkup

Es gibt einige Inlinemarkups.
Die simplesten sind im folgenden Beispiel aufgezeigt.

```markdown
Normaler Text
**Fettgedruckter Text**
__Fettgedruckter Text__
*Kursiv gesetzter Text*
_Kursiv gesetzter Text_
~~Durchgestichener Text~~

"Schöne doppelte Anführungszeichen"
'Schöne einfache Anführungszeichen'

Superscript: 19^th^
Subscript: H~2~O
Hinweis: Superscript und Subscript ~funktioniert nur~ ohne ^Leerzeichen dazwischen^. Es sei denn ~man\ escaped~ ^die\ Leerzeichen^.

Geviertstriche werden mit zwei oder drei Minuszeichen gemacht:
Schön---nein, unfassbar!
Während 1980--1988.

***Gemischte*** ~~_Syntax_~~ __~geht~__ **_^auch^_**
```
:::success
Normaler Text
**Fettgedruckter Text**
__Fettgedruckter Text__
*Kursiv gesetzter Text*
_Kursiv gesetzter Text_
~~Durchgestichener Text~~

"Schöne doppelte Anführungszeichen"
'Schöne einfache Anführungszeichen'

Superscript: 19^th^
Subscript: H~2~O
Hinweis: Superscript und Subscript ~funktioniert nur~ ohne ^Leerzeichen dazwischen^. Es sei denn ~man\ escaped~ ^die\ Leerzeichen^.

Geviertstriche werden mit zwei oder drei Minuszeichen gemacht:
Schön---nein, unfassbar!
Während 1980--1988.

***Gemischte*** ~~_Syntax_~~ __~geht~__ **_^auch^_**
:::

:::danger
TODO
https://pandoc.org/MANUAL.html#underline
https://pandoc.org/MANUAL.html#small-caps
https://pandoc.org/MANUAL.html#extension-smart
https://pandoc.org/MANUAL.html#emphasis
https://pandoc.org/MANUAL.html#highlighting
https://pandoc.org/MANUAL.html#strikeout
https://pandoc.org/MANUAL.html#superscripts-and-subscripts
:::
    "Strong"     :{"handler" : InlineSimple, "etype":"strong"     },
    "Emph"       :{"handler" : InlineSimple, "etype":"emph"       },
    "Underline"  :{"handler" : InlineSimple, "etype":"underline"  },
    "Strikeout"  :{"handler" : InlineSimple, "etype":"strikeout"  },
    "Superscript":{"handler" : InlineSimple, "etype":"superscript"},
    "Subscript"  :{"handler" : InlineSimple, "etype":"subscript"  },
    "SmallCaps"  :{"handler" : InlineSimple, "etype":"smallcaps"  },
    "Quoted"     : InlineQuoted,
    "SoftBreak"  : InlineSoftBreak,
    "LineBreak"  : InlineLineBreak,

Die restlichen Inlinemarkups sind in den folgenden Unterabschnitten genauer beschrieben.

### Links
:::danger
TODO
https://pandoc.org/MANUAL.html#links-1
https://pandoc.org/MANUAL.html#extension-abbreviations
https://pandoc.org/MANUAL.html#extension-implicit_header_references
:::
    "Link"       : InlineLink,

### Bilder
:::danger
TODO
https://pandoc.org/MANUAL.html#images
:::
    "Image"      : InlineImage,

### :point_right: Emoji :point_left:

Es gibt Emojis! :smiley:
Eine Auflistung *aller* 1855 möglichen Emojis findest du in der [Emojiliste](slug:emojis).

```markdown
Fühl dich gedrückt :hugs: :hearts:
```
:::success
Fühl dich gedrückt :hugs: :hearts:
:::


<!-- https://pandoc.org/MANUAL.html#extension-emoji -->

### Mathe
:::danger
TODO
https://pandoc.org/MANUAL.html#math-input
https://pandoc.org/MANUAL.html#math
https://pandoc.org/MANUAL.html#extension-tex_math_single_backslash
:::
    "Math"       : InlineMath,

### Code
:::danger
TODO https://pandoc.org/MANUAL.html#verbatim
:::


### Fußnoten
:::danger
TODO
https://pandoc.org/MANUAL.html#footnotes
:::
    "Note"       : InlineFootnote,

### Backslash-Escape

Außer in [Codeblöcken](#codeblocke) oder [Inline Code](#code), werden alle Zeichensetzungen oder Leerzeichen, welche direkt auf ein Backslash folgen, als Literale gelesen.

```markdown
*Dieser Text ist kursiv.*
\*Dieser Text hat Sternchen.\*
*\*Dieser Text hat Sternchen und ist kursiv.\**
```
:::success
*Dieser Text ist kursiv.*
\*Dieser Text hat Sternchen.\*
*\*Dieser Text hat Sternchen und ist kursiv.\**
:::

:::danger
TODO
https://pandoc.org/MANUAL.html#extension-all_symbols_escapable
:::

### Nonbreaking Space
:::danger
TODO
https://pandoc.org/MANUAL.html#extension-smart
https://pandoc.org/MANUAL.html#extension-all_symbols_escapable
:::

## Alerts
:::danger
TODO
:::

## Zitate
:::danger
TODO
https://pandoc.org/MANUAL.html#block-quotations
:::
    "BlockQuote"    : BlockQuote,

## Listen
:::danger
TODO
https://pandoc.org/MANUAL.html#extension-lists_without_preceding_blankline
:::

### Ungeordnete Listen
:::danger
TODO
https://pandoc.org/MANUAL.html#bullet-lists
:::
    "BulletList"    : BlockBulletList,
### Geordnete Listen
:::danger
TODO
https://pandoc.org/MANUAL.html#ordered-lists
:::
    "OrderedList"   : BlockOrderedList,
### Aufgabenlisten
:::danger
TODO
https://pandoc.org/MANUAL.html#extension-task_lists
:::

### Beispiellisten
:::danger
TODO
https://pandoc.org/MANUAL.html#numbered-example-lists
:::

### Listen beenden
:::danger
TODO
https://pandoc.org/MANUAL.html#ending-a-list
:::

## Definitionslisten
:::danger
TODO
https://pandoc.org/MANUAL.html#definition-lists
:::
    "DefinitionList": BlockDefinitionList,

## Codeblöcke
:::danger
TODO
https://pandoc.org/MANUAL.html#verbatim-code-blocks
:::
    "CodeBlock"     : BlockCode,

## Horizontale Trennlinien

:::danger
TODO
https://pandoc.org/MANUAL.html#horizontal-rules
:::
    "HorizontalRule": BlockHorizontalRule,

## Tabellen
:::danger
TODO
https://pandoc.org/MANUAL.html#tables
:::
    "Table"         : BlockTable,


## Kommentare

Kommentare können einfach mit [HTML Kommentaren](https://www.w3schools.com/html/html_comments.asp) gemacht werden.

```markdown
Hallo Welt
<!-- Ich bin unsichtbar. Achtung: Es kann zusätzliche Zeilenumbrüche geben. -->
Guten Tag
```
:::success
Hallo Welt
<!-- Ich bin unsichtbar. Achtung: Es kann zusätzliche Zeilenumbrüche geben. -->
Guten Tag
:::

## Divs und Spans
:::danger
TODO
https://pandoc.org/MANUAL.html#divs-and-spans
https://pandoc.org/MANUAL.html#generic-raw-attribute
https://pandoc.org/MANUAL.html#raw-htmltex
:::
    "Div"           : BlockContainer,
    "Span"       : InlineContainer,

### Sachen nebeneinander darstellen

:::danger
TODO
https://pandoc.org/MANUAL.html#columns
:::



## Rohes HTML
:::danger
TODO
https://pandoc.org/MANUAL.html#raw-html
https://pandoc.org/MANUAL.html#raw-htmltex
:::
    "RawBlock"      : BlockRaw,
    "RawInline"  : InlineRaw,

## Abbreviations
Nicht unterstützt.

:::danger
TODO
https://pandoc.org/MANUAL.html#extension-abbreviations
:::



## Line Blocks
:::danger
TODO
https://pandoc.org/MANUAL.html#line-blocks
:::



















