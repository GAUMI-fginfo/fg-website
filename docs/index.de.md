---
title: Dokumentation
template: "index.html"
after:
  - type: "news"
    title:
      de: "Zuletzt Veränderte Seiten"
    id: "last_changed"
    num: 5

---

Hier findest du die Dokumentation zur Fachgruppenwebseite.

