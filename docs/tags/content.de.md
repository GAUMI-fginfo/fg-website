---
title: "Inhalte Erstellen"
color: "#22bb44"
slug: "tag-content"
lang: de
---

Hier ist die Dokumentation zum Erstellen von Inhalten.
Quasi alle Informationen rund ums [Inhaltsrepo](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data).


